var FS = require('fs');
var MARKOV = require('markoff');
var MARK = new MARKOV();

var Bot  = require('slackbots');

/******************************************************************************/

var SlackContext = module.exports = function SlackContext(api, raw) {
    if (!(this instanceof SlackContext))
        return new SlackContext(api, raw);

    var self = this;

    self.api = api;
    self.raw = raw;
};

SlackContext.prototype.send_pm = function (user, content) {
    var self = this;

    self.api.bot.postMessageToUser(user, content);
};

SlackContext.prototype.respond = function (content) {
    var self = this;

};

/******************************************************************************/

var SlackAPI = module.exports = function SlackAPI(mgr, key, config, creds, vault) {
    if (!(this instanceof SlackAPI))
        return new SlackAPI(mgr, cfg);

    var self = this;

    self.manager = mgr;
    self.alias   = key;

    self.config  = config;
    self.creds   = creds;
    self.vault   = vault;

    try {
        self.catalog = FS.readFileSync(SAVEFILE, 'UTF8').trim().split('\n');
    } catch (ERRRRROR) {
        self.catalog = [];
    }
};

SlackAPI.prototype.prepare = function () {
    var self = this;

    self.bot = new Bot({
        token: self.creds.api_token,
        name:  self.config.bot_name,
    });

    /******************************************************************************/

    self.bot.on('start', function (ev) {
        self.handle_start(new SlackContext(self, ev));
    });

    self.bot.on('message', function (data) {
        if (data.user == self.bot.id) return; // Ignore bot's own messages
     
        // More goes here later..
        switch (data.type) {
            case 'message':
                var cnt = new SlackContext(self, data);

                self.handle_message(cnt);
                break;
            default:
                console.log(data);
                break;
        }
    });
};

SlackAPI.prototype.lunch = function () {
    var self = this;

    //self.bot.start();
};

SlackAPI.prototype.handle_start = function (cnt) {
    var self = this;

    cnt.send_pm('shivhack', "I'm back online!");
};

SlackAPI.prototype.handle_message = function (cnt) {
    var self = this;

    Feel.parse(cnt.raw.text, function (err, raw, vibe) {
        if (cnt.raw.text=='hello') {
            cnt.api.bot.postMessage(cnt.raw.channel, 'Hello back !');
        } else {
            if (vibe!=null) {
                cnt.api.bot.postMessage(cnt.raw.channel, JSON.stringify(vibe));
            } else {
                console.log(cnt.raw, vibe);
            }
        }
    });
};

